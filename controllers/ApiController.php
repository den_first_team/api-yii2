<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\Response;


class ApiController extends Controller
{
    public function init() {
        Yii::$app->response->format = Response::FORMAT_JSON;
    }

    public function actionIndex()
    {
        return [];
    }
    
    public function actionAuth(){
        $key = isset($_FILES['file']['tmp_name']) && is_file($_FILES['file']['tmp_name']) ? trim((string)file_get_contents($_FILES['file']['tmp_name'])) : '';
        if (!is_string($key) || strlen($key)>60)
            $key = '';
        $data = ['authKey'=>$key];
        if (isset($_POST['password']))
            $data['password'] = (string)$_POST['password'];
        $comp = Yii::$app->ztl_api->setModel('User')->setJsonData($data)->setAction('authAPI')->setId_name('authKey')->load();
        return [
            'status' => $comp -> getStatus(),
            'message' => $comp -> getMsg(),
            'data' => $comp -> getData(),
       ];
    }
    
    public function actionEnter_password(){
        $comp = Yii::$app->ztl_api->setModel('User')->setAction('updateAPI')->setId_name('token')->accessСheck()->load()->convertMsg('enter_password');
        return [
            'status' => $comp -> getStatus(),
            'message' => $comp -> getMsg(),
            'data' => $comp -> getData(),
        ];
    }

    public function actionFarm($id=null){
       $comp = Yii::$app->ztl_api->setModel('farms')->setAction($id.'API')->setLogs()->accessСheck()->load(); 
       return [
            'status' => $comp -> getStatus(),
            'message' => $comp -> getMsg(),
            'data' => $comp -> getData(),
        ];
    }
    
    public function actionDriver($id=null){
       $comp = Yii::$app->ztl_api->setModel('drivers')->setAction($id.'API')->accessСheck()->load();
       return [
            'status' => $comp -> getStatus(),
            'message' => $comp -> getMsg(),
            'data' => $comp -> getData(),
        ];
    }
    
    public function actionUser($id=null){
       $comp = Yii::$app->ztl_api->setModel('User')->setAction($id.'API')->accessСheck(['userManagement'=>1])->load();
       return [
            'status' => $comp -> getStatus(),
            'message' => $comp -> getMsg(),
            'data' => $comp -> getData(),
        ];
    }
    
    public function actionRoad($id=null){
        $comp = Yii::$app->ztl_api->setModel('Roads')->setAction($id.'API')->setLogs()->accessСheck()->load();
        return [
            'status' => $comp -> getStatus(),
            'message' => $comp -> getMsg(),
            'data' => $comp -> getData(),
        ];
    }


    public function actionReg(){
        $comp = Yii::$app->ztl_api->setModel('User')->setAction('createAPI')->accessСheck(['userManagement'=>1])->load();
        return [
            'status' => $comp -> getStatus(),
            'message' => $comp -> getMsg(),
            'data' => $comp -> getData(),
        ];
    }
    
    
    
    
    
}
