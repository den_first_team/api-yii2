<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\Response;
/**
 * Description of UploadController
 *
 * @author programmer_5
 */
class DownloadController extends Controller {
    
//    public function actionIndex(){
//        die('er');
//    }


    public function actionKey($id=null){
        $f = Yii::getAlias('@app').'/web/key_files/'.$id.'.txt';
        if (is_file($f)){
            Yii::$app->response->sendFile($f);
            unlink($f);
        }
        else
            die('File not exist');
    }
}
