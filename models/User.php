<?php

namespace app\models;

use Yii;

class User extends Users implements \yii\web\IdentityInterface
{
   
    private $fileKey;

    
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['token' => $token]);
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($login,$key='authKey')
    {
        return static::findOne([$key => $login]);
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
       // dump($password,1);
       return Yii::$app->getSecurity()->validatePassword($password, $this->password);
    }
    
    public function setPassword($password=''){
       $this->password = $password ? Yii::$app->security->generatePasswordHash($password) : '';
    }
    
    public function setAuthKey(){
        $this->authKey =  Yii::$app->security->generateRandomString().'_auth_key';
        $this->saveKeyFile();
    }
    
    public function setToken(){
        if (!$this->notUpdateToken)
             $this->token = Yii::$app->security->generateRandomString().'_token';
    }

    public function permissionsFields(){
        return ['logistic','farm','dealings','tasks','userManagement'];
    }
    
    public function getPermissions(){
        $permissions = $this->convertObjToArray($this->permissionsFields());
        $res = [];
        foreach ($permissions as $k=>$v){
            if ($v)
                $res[] = $k;
        }
        return $res;
    }
    
    public function setPermissions($data=null){
        foreach ($this -> permissionsFields() as $v){
            $this->$v = $this->$v ? '1' : '0';
            if ($data){
                $this->$v = isset($data[$v]) && $data[$v] ? '1' : '0';
            }
        }
        return $this;
    }

    public function getKeyFileUrl(){
        
        return $this->fileKey ? 'http://zt-logic.com/download/key/'.$this->fileKey : false;
    }

    public function saveKeyFile(){
        $this->fileKey = uniqid($this->id);
        $f = Yii::getAlias('@app').'/web/key_files/'.$this->fileKey.'.txt';
        file_put_contents($f, $this->authKey);
    }
    
    public function getUserInfo($fields=['id','firstName','lastName','phoneNumber','permissions']){
        $data = $this->convertObjToArray($fields);
        if (isset($fields['permissions']))
            $data['permissions'] = $this->convertObjToArray($this->permissionsFields());
        if ($this->getKeyFileUrl())
            $data['KeyFileUrl'] = $this->getKeyFileUrl();
        return $data;
    }


    public function beforeSave($insert) {
        parent::beforeSave($insert);
        $u = self::findByUsername($this->phoneNumber, 'phoneNumber');
        return true;
    }

        public function updatePassword($pass='',$confirmPass=''){
        if ($pass == $confirmPass){
            $this->setPassword ($pass);
            return true;
        } else
            return false;
    }
    
    public function saveUser($obj,$data=null){
        if (!$obj)
            return ['status' => 0,'msg' => 'User is not exist','data' => null];
        if (isset($data['token']))
            unset($data['token']);
        $obj->attributes = $data;
        if (isset($data['permissions'])){
            $obj->attributes = $data['permissions'];
            $obj->setPermissions($data['permissions']);
        }
        
        if (isset($data['password']))
            $obj->setPassword($data['password']);
        if (!$obj->token || isset($data['changeToken']))
            $obj->setToken();
        if (!$obj->authKey || isset($data['changeAuthKey'])){
            $obj->setAuthKey();
        }
        $obj->save();
        if ($obj->getErrors())
            return ['status' => 0,'msg' => $obj->getErrorsMsg(),'data' => null];
        else { 
            return ['status' => 1,'msg' => 'User successfully saved','data' => $obj -> getUserInfo()];
        }
    }

    public function createAPI($obj){
        return $this->saveUser($this,$obj->getJsonData());
    }
    
    public function updateAPI($obj) {
        $userInf = $this->find()->where([$obj->getId_name() => $obj->getId()])->one();
        return $this->saveUser($userInf,$obj->getJsonData());
    }

    public function authAPI($obj){
        $data = $obj->getJsonData();
     //   dump($data,1,false);
        $login = isset($data['login']) ? (string)$data['login'] : $obj->getId();
        $key = isset($data['login']) ? 'phoneNumber' : 'authKey';
        $inf = $this -> findByUsername($login,$key);
        if (!$inf)
            return ['status' => 0,'msg' => 'the key is incorrect','data' => null];
        $inf->setToken();
        $res = ['status' => 1,'msg' => 'successful authorization','data' => $inf -> convertObjToArray(['token'])];
        if ($inf->password){
            if (!isset($data['password']) || !trim($data['password']) || !$inf ->validatePassword($data['password'])) 
                $res = ['status' => 0,'msg' => 'the password is incorrect','data' => null];
             else 
                $res['data']['permissions'] = $inf -> getPermissions();
        } else {
            $res['msg'] = 'Successful authorization. No password';
            $res['data']['enter_password'] = true;
        }
        $inf -> save();
        return $res;
    }
    
    public function readAPI($obj){
       $userInfo = $this->find()->where(['id'=>$obj->getId()])->one();
       if ($userInfo)
           return ['status' => 1,'msg' => 'Success','data' => $userInfo->getUserInfo()];
       
       return ['status' => 0,'msg' => 'User not exist','data' => null];
    }
//    
    public function listAPI($obj){
       $users = $this->find()->all();
       $res=['status' => 1,'msg' => 'Success','data' => null];
       if ($users)
           foreach ($users as $k=>$v)
               $res['data'][$k] = $v-> getUserInfo();
       return $res;    
    }

    
 


    
    
}
