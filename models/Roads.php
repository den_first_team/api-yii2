<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "roads".
 *
 * @property int $id
 * @property int $driverID
 * @property int $loadingFarmID
 * @property int $loadingFarmPointID
 * @property int $unloadingFarmID
 * @property int $unloadingFarmPointID
 * @property int $cultureID
 * @property string $weight
 * @property string $deficit
 * @property string $contract
 * @property string $ttnNumber
 * @property string $price
 * @property int $statusID
 */
class Roads extends ActiveRecord {

  //  public $drivers;


    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'roads';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['driverID','senderFarmID', 'senderFarmPointID', 'receiverFarmID', 'receiverFarmPointID', 'cultureID', 'contract', 'ttnNumber', 'price'], 'required'],
            [['unloadingWeight','loadingWeight','driverID', 'senderFarmID', 'senderFarmPointID', 'receiverFarmID', 'receiverFarmPointID', 'cultureID', 'statusID','price'], 'integer'],
            [['contract', 'ttnNumber'], 'string', 'max' => 250],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'driverID' => 'Driver ID',
            'senderFarmID' => 'Loading Farm ID',
            'senderFarmPointID' => 'Loading Farm Point ID',
            'receiverFarmID' => 'Unloading Farm ID',
            'receiverFarmPointID' => 'Unloading Farm Point ID',
            'cultureID' => 'Culture ID',
            'loadingWeight' => 'Weight',
            'unloadingWeight' => 'Weight',
            'contract' => 'Contract',
            'ttnNumber' => 'Ttn Number',
            'price' => 'Price',
            'statusID' => 'Status ID',
        ];
    }

    public function fields() {
        return ['id', 'loadingWeight', 'unloadingWeight', 'deficit', 'contract', 'ttnNumber', 'price', 'driver', 'sender', 'receiver', 'culture', 'status'];
    }
    
    public function getFarmFields(){
        return ['id','name'];
    }
    
    public function getFarmPointFields(){
        return ['id','name'];
    }


//    public function ApiFields(){
//        
//    }

    /////////// RELATIONS  START ////////////////
    //Relations list
    public function relationsList() {
        return ['driverRelations', 'cultureRelations', 'statusRelations', 'senderFarmRelations', 'senderFarmPointRelations', 'receiverFarmRelations', 'receiverFarmPointRelations'];
    }

    //driverRelations
    public function getDriverRelations() {
        return $this->hasOne(Drivers::className(), ['id' => 'driverID']);
    }

    //cultureRelations
    public function getCultureRelations() {
        return $this->hasOne(Culture::className(), ['id' => 'cultureID']);
    }

    //statusRelations
    public function getStatusRelations() {
        return $this->hasOne(RoadStatuses::className(), ['id' => 'statusID']);
    }

    //senderFarmRelations
    public function getSenderFarmRelations() {
        return $this->hasOne(Farms::className(), ['id' => 'senderFarmID']);
    }

    //senderFarmPointRelations
    public function getSenderFarmPointRelations() {
        return $this->hasOne(FarmPoints::className(), ['id' => 'senderFarmPointID']);
    }

    //receiverFarmRelations
    public function getReceiverFarmRelations() {
        return $this->hasOne(Farms::className(), ['id' => 'receiverFarmID']);
    }

    //receiverFarmPointRelations
    public function getReceiverFarmPointRelations() {
        return $this->hasOne(FarmPoints::className(), ['id' => 'receiverFarmPointID']);
    }

    /////////// RELATIONS  END ////////////////
    /////////// GETERS START /////////

    public function getDeficit() {
        return (int) $this->loadingWeight - (int) $this->unloadingWeight;
    }

    //driver
    public function getDriver() {
        return $this->driverRelations ? $this->driverRelations->toArray() : null;
    }

    //culture
    public function getCulture() {
        return $this->cultureRelations ? $this->cultureRelations->toArray() : null;
    }

    //status
    public function getStatus() {
        return $this->statusRelations ? $this->statusRelations->toArray(['id', 'name']) : null;
    }

    //loading
    public function getSender() {
        $res = $this->senderFarmRelations ? $this->senderFarmRelations->toArray($this->farmFields) : null;
        if ($res)
            $res['point'] = $this->senderFarmPointRelations ? $this->senderFarmPointRelations->toArray($this->farmPointFields) : null;
        return $res;
    }

    public function getReceiver() {
        $res = $this->receiverFarmRelations ? $this->receiverFarmRelations->toArray($this->farmFields) : null;
        if ($res)
            $res['point'] = $this->receiverFarmPointRelations ? $this->receiverFarmPointRelations->toArray($this->farmPointFields) : null;
        return $res;
    }

    public function getRoads($fields = []) {
        $roads = self::find()->with($this->relationsList())->all();
        return $roads ? \yii\helpers\ArrayHelper::toArray($roads) : [];
    }

    public function getInfo() {
        $drivers = Drivers::find()->leftJoin('roads', 'roads.driverID = drivers.id')->where('roads.id is null')->asArray()->all();
        $cultures = Culture::find()->asArray()->all();
        $roadStatuses = RoadStatuses::find()->select(['id', 'name'])->asArray()->all();
        $farmsObj = Farms::find()->with('farmPoints')->all();
        $farms = ['receiver' => [], 'sender' => []];
        if ($farmsObj) {
            foreach ($farmsObj as $k => $v) {
                   if ($points = $v->points['receiver'])
                       $farms['receiver'][] = $v->toArray(['id','name']) + ['points'=>$points];
                   if ($points = $v->points['sender'])
                       $farms['sender'][] = $v->toArray(['id','name']) + ['points'=>$points];
            }
        }
        return [
            'drivers' => $drivers,
            'cultures' => $cultures,
            'roadStatuses' => $roadStatuses,
            'farms' => $farms
        ];
    }

    //  public function 
    ////////// GETERS END ////////////
    ///////// API METHODS START///////


    public function createAPI($obj) {
        return $this->saveRoad($obj->getJsonData());
    }

    public function updateAPI($obj) {
        $road = Roads::find()->innerJoin('roadStatuses','roadStatuses.id = roads.statusID AND roadStatuses.completed != 1')->where([$obj->getId_name() => $obj->getId()])->one();
        if ($road)
            return $road->saveRoad($obj->getJsonData());
        else
            return ['status' => 0, 'msg' => 'Rad with such id was not found!', 'data' => null];
    }
    
    public function listApi($obj){
        return ['status' => 1,'msg' => 'Success.','data' => ['info' => $this->getInfo(),'roads' => $this->getRoads()]];
    }
    
    public function historyApi(){
        $roads = Roads::find()->innerJoin('roadStatuses','roadStatuses.id = roads.statusID AND roadStatuses.completed = 1') -> with($this->relationsList())->all();
        $roads = $roads ? \yii\helpers\ArrayHelper::toArray($roads) : [];
        return ['status' => 1,'msg' => 'Success.','data' => $roads];
    }

    ///////// API METHODS END///////
    ///////// API OTHER METHODS START //////

    public function setPrice($price){
        $this->price = (float)$price;
    }

    public function setAttributes($values, $safeOnly = true) {
        
        if (isset($values['driver']['id']))
            $values['driverID'] = $values['driver']['id'];
      
        ///////sender
        if (isset($values['sender']['id']))
            $values['senderFarmID'] = $values['sender']['id'];
        
        if (isset($values['sender']['point']['id']))
            $values['senderFarmPointID'] = $values['sender']['point']['id'];
        
        ///////receiver
        if (isset($values['receiver']['id']))
            $values['receiverFarmID'] = $values['receiver']['id'];
        
        if (isset($values['receiver']['point']['id']))
            $values['receiverFarmPointID'] = $values['receiver']['point']['id'];
        
        ////culture
        
        if (isset($values['culture']['id']))
            $values['cultureID'] = $values['culture']['id'];
        
        
        return parent::setAttributes($values, $safeOnly);
    }
    
    public function beforeSave($insert) {
        if (!$this->statusID && $statusDefault = RoadStatuses::findOne(['defaultStatus'=>1]))
                $this->statusID = $statusDefault->id;
        return true;
    }

    public function saveRoad($data = null) {
        $res=[];
        if (isset($data['drivers']) && is_array($data['drivers']) && $driverArr = $data['drivers']) {
            $i=0;
            foreach ($driverArr as $v){
               $data['driver'] = $v;
               if (isset($v['id'])){
                   if (!$saveObj = Roads::find()->innerJoin('roadStatuses','roadStatuses.id = roads.statusID AND roadStatuses.completed != 1')->where(['driverID'=> $v['id']])->one())
                           $saveObj = $i == 0 ? $this : new Roads ();
                   $saveObj -> attributes = $data;
                   if ($saveObj->save())
                       $res[] = $saveObj->toArray ();
                    $i++;
               }
            }
        } else if ($this->save())
            $res[] = $this->toArray();
        
        if ($this->getErrors())
            return ['status' => 0, 'msg' => $this->getErrorsMsg(), 'data' => null];
        else
            return ['status' => 1, 'msg' => 'Road  successfully saved.', 'data' => $res];
    }

    ///////// API OTHER METHODS END //////
}
