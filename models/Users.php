<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property string $name
 * @property string $password
 * @property string $phone_number
 * @property string $token
 * @property string $auth_key
 * @property int $admin
 * @property int $logistic
 * @property string $remember_token
 * @property string $created_at
 * @property string $updated_at
 */
class Users extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['firstName','lastName', 'phoneNumber'], 'required'],
            [['created', 'updated'], 'safe'],
            [['firstName','lastName', 'password', 'phoneNumber', 'token'], 'string', 'max' => 200],
            [['authKey'], 'string', 'max' => 250],
          //  [['admin', 'logistic'], 'string', 'max' => 1],
          //  [['remember_token'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'firstName' => 'First name',
            'lastName' => 'Last name',
            'password' => 'Password',
            'phoneNumber' => 'Phone Number',
            'token' => 'Token',
            'authKey' => 'Auth Key',
            'admin' => 'Admin',
            'logistic' => 'Logistic',
            'created' => 'Created At',
            'updated' => 'Updated At',
        ];
    }
}