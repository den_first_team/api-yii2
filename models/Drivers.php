<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "drivers".
 *
 * @property int $id
 * @property string $phone
 * @property string $name
 * @property string $car_brand
 * @property string $car_number
 * @property string $car_type
 * @property string $personal_phone
 * @property string $company
 * @property string $coordinates
 * @property string $additional
 * @property string $status
 * @property int $active
 * @property string $created_at
 * @property string $updated_at
 */
class Drivers extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'drivers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['phone', 'name', 'carBrand', 'carNumber', 'carType', 'personalPhone', 'company'], 'required'],
            [['created', 'updated'], 'safe'],
            [['phone', 'name', 'carBrand', 'carType', 'personalPhone', 'company', 'coordinates', 'additional', 'status'], 'string', 'max' => 200],
            [['active'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'phone' => 'Phone',
            'name' => 'Name',
            'carBrand' => 'Car Brand',
            'carNumber' => 'Car Number',
            'carType' => 'Car Type',
            'personalPhone' => 'Personal Phone',
            'company' => 'Company',
            'coordinates' => 'Coordinates',
            'additional' => 'Additional',
            'status' => 'Status',
            'active' => 'Active',
            'created' => 'Created At',
            'updated' => 'Updated At',
        ];
    }
   
    public function beforeValidate(){
        $this->active = $this->active ? '1' : '0';
        return true;
    }
    
     /////////// RELATIONS  START ////////////////
    
    //Relations list
    public function relationsList(){ 
        return ['roadRelation'];
    }
    
    public function getRoadRelation (){
        return $this->hasOne(Roads::className(), ['driverID' => 'id']);
    }
    /////////// RELATIONS  END ////////////////
   
    
}
