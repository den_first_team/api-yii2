<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "roadStatuses".
 *
 * @property int $id
 * @property string $name
 * @property string $addInfo
 * @property int $completed
 */
class RoadStatuses extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'roadStatuses';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'addInfo'], 'required'],
            [['addInfo'], 'string'],
            [['name'], 'string', 'max' => 250],
            [['completed'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'addInfo' => 'Add Info',
            'completed' => 'Completed',
        ];
    }
}
