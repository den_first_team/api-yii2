<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "farms".
 *
 * @property int $id
 * @property string $label
 * @property string $contact_name
 * @property string $contact_phone
 * @property string $description
 * @property string $created_at
 * @property string $updated_at
 */
class Farms extends ActiveRecord
{
    
   
    public $pointName;



    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'farms';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'contactPhone'], 'required'],
            [['description'], 'string'],
            [['created', 'updated'], 'safe'],
            [['name', 'contactName', 'contactPhone'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Label',
            'contactName' => 'Contact Name',
            'contactPhone' => 'Contact Phone',
            'description' => 'Description',
            'created' => 'Created At',
            'updated' => 'Updated At',
        ];
    }   
    
    public function fields() {
        return ['id','name','farmPoints','points'];
    }


    public function getFieldsFarm(){
        return ['id','name','contactName','contactPhone','description','farmPoints'];
    }
    
    public function getFieldsPoints(){
        return ['id','name','receiver','sender'];
    }
    
    
    public function getFarmPoints()
    {
        return $this->hasMany(FarmPoints::className(), ['farmId' => 'id']);
    }
    
    public function getPoints(){
        $res = ['receiver'=>[],'sender' => []];
        if ($this->farmPoints)
               foreach ($this->farmPoints as $k=>$v){
                   if ($v->receiver == 1)
                       $res['receiver'][] = $v->convertObjToArray($this->getFieldsPoints());
                   if ($v->sender == 1)
                       $res['sender'][] = $v->convertObjToArray($this->getFieldsPoints());
               }
        return isset($res[$this->pointName]) ? $res[$this->pointName] : $res;
    }
    
    public function all(){
        $res = [];
        while ($arr = self::find()->all())
              $res[] = $arr->convertObjToArray($this->getFieldsPoints());
        return $res;
    }
    
    public function one(){
        $obj = self::find()->one();
        return $obj->convertObjToArray($this->getFieldsPoints());
    }


//    public function getPoints($fields){
//        $res = ['reciver'=>[],'sender' => []];
//        if ($this->farmPoints)
//               foreach ($this->farmPoints as $k=>$v){
//               
//                   if ($v->receiver == 1)
//                       $res['reciver'] = $v->convertObjToArray($fields);
//                   if ($v->sender == 1)
//                       $res['sender'] = $v->convertObjToArray($fields);
//        }
//        return $res;
//    }
    
    public function getFarm($fields=['farm'=>['id','name','contactName','contactPhone','description'],'farmPoints'=>['id','name','receiver','sender']]){
        $data = $this->convertObjToArray(isset($fields['farm']) ? $fields['farm'] : null);
        if (isset($fields['farmPoints'])){
           $data['receiver']=[];
           $data['sender'] = [];
           if ($this->farmPoints)
               foreach ($this->farmPoints as $k=>$v){
               
                   if ($v->receiver == 1)
                       $data['receiver'][] = $v->convertObjToArray($fields['farmPoints']);
                   if ($v->sender == 1)
                       $data['sender'][] = $v->convertObjToArray($fields['farmPoints']);
               }
        }
        return $data;
    }
    
    public function saveFarm($data){
        
        $this->attributes = $data;
        $this->save();
        if ($this->getErrors())
            return ['status' => 0,'msg' => $this->getErrorsMsg(),'data' => null];
        $res = ['status' => 1,'msg' => 'Farm  successfully saved.'];
        if (isset($data['farmPoints']) && is_array($data['farmPoints']))
           (new FarmPoints) ->saveData ($data['farmPoints'],$this->id);
        $res['data'] = $this->getFarm();
        return $res;
    }

    public function createAPI($obj){
        return $this->saveFarm($obj->getJsonData());
    }
    
    public function updateAPI($obj) {
        $farm = $this->find()->where([$obj->getId_name() => $obj->getId()])->one();
        if ($farm)
            return $farm->saveFarm($obj->getJsonData());
        else
            return ['status'=>0,'msg'=>'FARM with such id was not found!','data'=>null];
    }


    public function listAPI(){
        $res = ['status' => 1,'msg' => 'Success.','data' =>[]];
        $obj = $this->find()->with(['farmPoints'])->orderBy('id ASC')->all();
        if ($obj){
            foreach ($obj as $v)
                $res['data'][] = $v->getFarm();
        }
        return $res;
    }
    
    public function readAPI($obj){
        $objFarm = $this->find()->with(['farmPoints'])->where([$obj->getId_name() => $obj->getId()])->one();
        if ($objFarm)
            return ['status' => 1,'msg' => 'Success.','data'=>$objFarm->getFarm()];
        else
            return ['status' => 0,'msg' => 'Farm with such id was not found!','data'=>null];
    }
    
    public function deleteAPI($obj){
        $objFarm = $this ->find()->where([$obj->getId_name() => $obj->getId()])->one();
        if (!$objFarm)
            return ['status' => 0, 'msg' => 'FARM with such id was not found!','data' => null];
        FarmPoints::deleteAll(['farmId'=>$objFarm->id]);
        $objFarm->delete();
        return [
               'status' => 1,
               'msg' => 'FARM successful deletion',
               'data' => null
           ]; 
    }
    
    
    //////////place for test methods START//////////
    
//    public function setName ($pointName){
//        $this->pointName = 'wegewfwefwefwefwefwefwefwefwefwef';
//    }


//     public function getName (){
//        return $this->pointName;
//    }

    
    //////////place for test methods END//////////
    
}



