<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\models;

use Yii;

/**
 * Description of ActiveRecord
 *
 * @author programmer_5
 */
class ActiveRecord  extends \yii\db\ActiveRecord {
    
    public function beforeSave($insert) {
        parent::beforeSave($insert);
        if (in_array('created',$this->attributes()) && !$this->created)
                $this->created = time();
        if (in_array('updated', $this->attributes()))
                $this->updated = time();
        return true;
    }
    
    public function getErrorsMsg(){
         $msg = ''; 
         if ($this->getErrors())
           foreach ($this->getErrors() as $k=>$v){
               $msg .= $k.': '.array_shift($v).'; ';
           } 
           return $msg; 
    }
    
    public function convertObjToArray($arr=null,$addAttr=null,$delAttr=null){
        $arr = $arr ? $arr : $this->attributes();
        if ($addAttr){
            foreach ($addAttr as $v){
                if (!in_array($v, $arr))
                        $arr[] = $v;
            }
        }
        
        if ($delAttr){
            foreach ($delAttr as $v){
                if (in_array($v, $arr))
                   unset($arr[array_search($v, $arr)]);     
            }
        }
        
        foreach ($arr as $v){
            $res[$v] = $this->$v;
        }
        return $res;
    }
    
    public function allArray($arr = null, $addAttr = null, $delAttr = null) {
        $objArr = self::find()->all();
        if ($objArr){
            foreach ($objArr as $k=>$v)
                $objArr[$k] = $v->convertObjToArray($arr, $addAttr, $delAttr);
        }
        return $objArr;
    }
    
    public function getSelections($obj=null,$attr,$id_attr='id'){
        $arr = is_object($obj) ? $obj->find()->asArray()->all() : $obj;
        if ($arr){
            foreach ($arr as $k=>$v){
                $arr[$k]['checked'] = $v[$id_attr] == $this->$attr ? true : false;
            }
        }
        return $arr;
    }
   
}
