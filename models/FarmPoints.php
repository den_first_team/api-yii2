<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "farm_points".
 *
 * @property int $id
 * @property string $label
 * @property int $receiver
 * @property int $sender
 * @property int $farm_id
 * @property string $created_at
 * @property string $updated_at
 */
class FarmPoints extends ActiveRecord
{
    
//   public $receiver = '0';
//   public $sender = '0';
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'farm_points';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'farmId'], 'required'],
            [['farmId'], 'integer'],
            [['created', 'updated'], 'safe'],
            [['name'], 'string', 'max' => 200],
            [['receiver', 'sender'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'receiver' => 'Receiver',
            'sender' => 'Sender',
            'farmId' => 'Farm ID',
            'created' => 'Created At',
            'updated' => 'Updated At',
        ];
    }
   
    public function beforeValidate(){
        $this->receiver = $this->receiver ? '1' : '0';
        $this->sender = $this->sender ? '1' : '0';
        return true;
    }
    
    public function saveData($data=[],$farmID=null){
        $farmPoints=$this -> find()->where(['farmId'=>$farmID])->all();
        $farmPointsConvert=[];
        if ($farmPoints)
            foreach ($farmPoints as $v)
                $farmPointsConvert[$v->id] = $v;
        $farmPointsSaved = [];
        if (is_array($data) && count($data)){
            foreach ($data as $v){
                $obj = isset($v['id']) && isset($farmPointsConvert[$v['id']]) ? $farmPointsConvert[$v['id']] : clone $this;
                $obj->attributes = $v+['farmId'=>$farmID];
                $obj->save();
                $farmPointsSaved[] = $obj->id;
            }
        }
        if ($farmPointsConvert){
            foreach ($farmPointsConvert as $v)
                if (!in_array($v->id, $farmPointsSaved))
                        $v->delete();
        }
    }
}